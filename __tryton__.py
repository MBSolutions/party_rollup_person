#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party Rollup Person',
    'name_de_DE': 'Parteien Struktur Personen',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
        - Relate persons to organizations
    ''',
    'description_de_DE': '''
        - Zuordnung von Personen zu Organisationen
    ''',
    'depends': [
        'party_type',
        'party_rollup',
    ],
    'xml': [
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
